# The Geo Game

A simple geography game I am developing as a playground for learning Rust.

## Current state

At the moment it is a simple guessing game on capitals of different countries.

This version packs everything inside the main.rs file.

## Branches

**This branch :** Master

Aims to populate the game and always keep a fully functional state of the game.

**Other branches :**
- geo-tui : Implement a fully functional Text User Interface (terminal-based) using [cursive framework ](https://github.com/gyscos/cursive/)
- ext-countries : Work on moving the country database to another file (e.g. out of main.rs)

## Development :

- Keep populating the game
- Fix Option to start over at end of game -> currently broken
