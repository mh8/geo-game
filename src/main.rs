// crates
extern crate rand;

// libs
use rand::seq::SliceRandom;
use std::fmt::{self, Formatter};
use std::io;

struct Country {
    id: usize,
    country: String,
    capital: String,
    user_guess: String,
    is_guessed: bool,
    is_correct: bool,
}

// Implement customized display
impl fmt::Display for Country {
    fn fmt(&self, f: &mut ::Formatter) -> fmt::Result {
        writeln!(
            f,
            "{}.{} - Capital : {}",
            self.id, self.country, self.capital
        )
        .expect("Something went wrong when trying to display the country");
        writeln!(
            f,
            "User guessed ({}) {} which is {}",
            self.is_guessed, self.user_guess, self.is_correct
        )
    }
}

const TOGGLE_DEBUG: bool = false;

fn main() {
    println!("Welcome to the Geography Game");
    println!("\nInstructions :");
    println!("1. When prompted try guessing the capital of the given country");
    println!("2. Type 'exit' to quit.");
    println!("3. Type 'list' to display all the countries.\n");

    //Create all countries
    //This should be stored in a seperate file and imported in
    let mut france = Country {
        id: 0,
        country: String::from("France"),
        capital: String::from("Paris"),
        user_guess: String::from(""),
        is_guessed: false,
        is_correct: false,
    };

    let mut spain = Country {
        id: 1,
        country: String::from("Spain"),
        capital: String::from("Madrid"),
        user_guess: String::from(""),
        ..france
    };

    let mut england = Country {
        id: 2,
        country: String::from("England"),
        capital: String::from("London"),
        user_guess: String::from(""),
        ..france
    };

    let mut belgium = Country {
        id: 3,
        country: String::from("Belgium"),
        capital: String::from("Bruxelles"),
        user_guess: String::from(""),
        ..france
    };

    let mut netherlands = Country {
        id: 4,
        country: String::from("Netherlands"),
        capital: String::from("Amsterdam"),
        user_guess: String::from(""),
        ..france
    };

    let mut luxembourg = Country {
        id: 5,
        country: String::from("Luxembourg"),
        capital: String::from("Luxembourg City"),
        user_guess: String::from(""),
        ..france
    };

    let mut germany = Country {
        id: 6,
        country: String::from("Germany"),
        capital: String::from("Berlin"),
        user_guess: String::from(""),
        ..france
    };

    let mut austria = Country {
        id: 7,
        country: String::from("Austria"),
        capital: String::from("Vienna"),
        user_guess: String::from(""),
        ..france
    };

    let mut italy = Country {
        id: 8,
        country: String::from("Italy"),
        capital: String::from("Rome"),
        user_guess: String::from(""),
        ..france
    };

    let mut portugal = Country {
        id: 9,
        country: String::from("Portugal"),
        capital: String::from("Lisbon"),
        user_guess: String::from(""),
        ..france
    };

    let mut rep_ireland = Country {
        id: 10,
        country: String::from("Republic of Ireland"),
        capital: String::from("Dublin"),
        user_guess: String::from(""),
        ..france
    };

    let mut nor_ireland = Country {
        id: 11,
        country: String::from("Northern Ireland"),
        capital: String::from("Belfast"),
        user_guess: String::from(""),
        ..france
    };

    let mut scotland = Country {
        id: 12,
        country: String::from("Scotland"),
        capital: String::from("Edinburgh"),
        user_guess: String::from(""),
        ..france
    };

    let mut wales = Country {
        id: 13,
        country: String::from("Wales"),
        capital: String::from("Cardiff"),
        user_guess: String::from(""),
        ..france
    };

    let mut denmark = Country {
        id: 14,
        country: String::from("Denmark"),
        capital: String::from("Copenhagen"),
        user_guess: String::from(""),
        ..france
    };

    let mut poland = Country {
        id: 15,
        country: String::from("Poland"),
        capital: String::from("Warsaw"),
        user_guess: String::from(""),
        ..france
    };

    let mut czechia = Country {
        id: 16,
        country: String::from("Czechia (Czech Republic)"),
        capital: String::from("Prague"),
        user_guess: String::from(""),
        ..france
    };

    let mut slovenia = Country {
        id: 17,
        country: String::from("Slovenia"),
        capital: String::from("Ljubljana"),
        user_guess: String::from(""),
        ..france
    };

    let mut croatia = Country {
        id: 18,
        country: String::from("Croatia"),
        capital: String::from("Zagreb"),
        user_guess: String::from(""),
        ..france
    };

    let mut hungary = Country {
        id: 19,
        country: String::from("Hungary"),
        capital: String::from("Budapest"),
        user_guess: String::from(""),
        ..france
    };

    let mut slovakia = Country {
        id: 20,
        country: String::from("Slovakia"),
        capital: String::from("Bratislava"),
        user_guess: String::from(""),
        ..france
    };

    let mut romania = Country {
        id: 21,
        country: String::from("Romania"),
        capital: String::from("Bucharest"),
        user_guess: String::from(""),
        ..france
    };

    let mut bulgaria = Country {
        id: 22,
        country: String::from("Bulgaria"),
        capital: String::from("Sofia"),
        user_guess: String::from(""),
        ..france
    };

    let mut greece = Country {
        id: 23,
        country: String::from("Greece"),
        capital: String::from("Athens"),
        user_guess: String::from(""),
        ..france
    };

    let mut switzerland = Country {
        id: 24,
        country: String::from("Switzerland"),
        capital: String::from("Bern"),
        user_guess: String::from(""),
        ..france
    };

    let mut lithuania = Country {
        id: 25,
        country: String::from("Lithuania"),
        capital: String::from("Vilnius"),
        user_guess: String::from(""),
        ..france
    };

    let mut latvia = Country {
        id: 26,
        country: String::from("Latvia"),
        capital: String::from("Riga"),
        user_guess: String::from(""),
        ..france
    };

    let mut estonia = Country {
        id: 27,
        country: String::from("Estonia"),
        capital: String::from("Tallinn"),
        user_guess: String::from(""),
        ..france
    };

    let mut finland = Country {
        id: 28,
        country: String::from("Finland"),
        capital: String::from("Helsinki"),
        user_guess: String::from(""),
        ..france
    };

    let mut sweden = Country {
        id: 29,
        country: String::from("Sweden"),
        capital: String::from("Stockholm"),
        user_guess: String::from(""),
        ..france
    };

    let mut norway = Country {
        id: 30,
        country: String::from("Norway"),
        capital: String::from("Oslo"),
        user_guess: String::from(""),
        ..france
    };

    let mut user_score: u16 = 0;
    let mut counter: usize = 0;

    let mut v: Vec<&mut Country> = Vec::new();

    //Load all countries
    v.push(&mut france);
    v.push(&mut spain);
    v.push(&mut england);
    v.push(&mut belgium);
    v.push(&mut netherlands);
    v.push(&mut luxembourg);
    v.push(&mut germany);
    v.push(&mut austria);
    v.push(&mut italy);
    v.push(&mut portugal);
    v.push(&mut rep_ireland);
    v.push(&mut nor_ireland);
    v.push(&mut scotland);
    v.push(&mut wales);
    v.push(&mut denmark);
    v.push(&mut poland);
    v.push(&mut czechia);
    v.push(&mut slovenia);
    v.push(&mut croatia);
    v.push(&mut hungary);
    v.push(&mut slovakia);
    v.push(&mut romania);
    v.push(&mut bulgaria);
    v.push(&mut greece);
    v.push(&mut switzerland);
    v.push(&mut lithuania);
    v.push(&mut latvia);
    v.push(&mut estonia);
    v.push(&mut finland);
    v.push(&mut sweden);
    v.push(&mut norway);

    let mut remaining_countries: Vec<usize> = Vec::new();

    //Populate the remaining_countries array with all the IDs of the countries to guess
    for i in &v {
        remaining_countries.push(i.id);
    }

    println!("Loaded {} countries.", v.len());

    //Check the IDs are all unique
    for j in 0..remaining_countries.len() {
        for k in 0..remaining_countries.len() {
            if k == j {
                //skip
            } else {
                if &remaining_countries[j] == &remaining_countries[k] {
                    panic!("Some ID is duplicate at position {} & {}", j, k);
                }
            }
        }
    }

    //begin a loop here, generate a number, test if the country has been guessed, if yes skip, if no ask to guess
    loop {
        if TOGGLE_DEBUG {
            println!("\n----------------------");
            println!("Starting loop\n");
        }
        //Generate a number amidst the remaining country ids to guess
        let token: usize;
        match remaining_countries.choose(&mut rand::thread_rng()) {
            Some(i) => token = *i,
            None => token = 0,
        }

        if TOGGLE_DEBUG {
            println!(
                "Countries ID vector populated with : {:?}",
                remaining_countries
            );
            println!("The random number is {}", token);
        }

        if counter < v.len() {
            if v[token].is_guessed == true {
                // If already guessed, skip
            } else if v[token].is_guessed == false {
                println!("\n====== {} / {} ======", counter + 1, v.len());
                println!("\nWhat is the capital of {} :", v[token].country);

                io::stdin()
                    .read_line(&mut v[token].user_guess)
                    .expect("Something went wrong. Program ended.");

                v[token].user_guess = v[token].user_guess.trim().to_string();
                v[token].is_guessed = true;

                // EXIT PROMPT
                if v[token].user_guess == String::from("exit") {
                    break;
                }

                // DEBUGGING PROMPT
                if v[token].user_guess == String::from("list") {
                    let mut a = 0;
                    while a < v.len() {
                        println!("{}", v[a]);
                        a += 1;
                    }
                    break;
                } else {
                    tester(&mut v[token], &mut user_score);

                    counter += 1;
                    println!(
                        "Score : {} / {}",
                        user_score,
                        v.len() - remaining_countries.len() + 1
                    );
                    println!("---------------------------\n");

                    //<--- Remove the country from the vector where we store remaining countries to guess
                    let mut marker = 0;

                    for i in 0..remaining_countries.len() {
                        if TOGGLE_DEBUG {
                            println!(">> i : {}", i);
                            println!(">>vtoken: {}", v[token].id);
                            println!(">>Remaining_countries[i] : {}", remaining_countries[i]);
                        }
                        if remaining_countries[i] == v[token].id {
                            marker = i;
                            if TOGGLE_DEBUG {
                                println!("Got a match on {}", i);
                            }
                        }
                    }
                    remaining_countries.remove(marker);
                    //<-- This could be sloppy code
                }
            }
        } else if counter == v.len() {
            let mut user_input = String::new();

            println!(
                "Good Game ! You finished with a score of {} / {}",
                user_score,
                v.len()
            );
            println!("Here is the list of the countries you missed :\n");
            let mut a = 0;
            while a<v.len() {
                if v[a].is_correct == false {
                    println!("{}", v[a]);
                }
                a+=1;
            }
            println!("Would you like to play again [y/n] ?");

            io::stdin()
                .read_line(&mut user_input)
                .expect("Something went wrong. Program ended.");

            //GAME RESET
            //Repopulate the remaining_countries vector and reset the guesses
            for i in &mut v {
                remaining_countries.push(i.id);
                i.is_guessed = false;
                i.user_guess = String::from("");
            }
            //And reset scores
            counter = 0;
            user_score = 0;
            println!("\n\n Great ! Let's start over. \n\n");
            continue;
        }
    }
}

fn tester(country: &mut Country, score: &mut u16) -> bool {
    if country.user_guess.to_lowercase() == country.capital.to_lowercase() {
        country.is_correct = true;
        println!("Correct !");
        println!("");
        *score += 1;
        return country.is_correct;
    } else {
        country.is_correct = false;
        println!("Wrong, the correct answer was : {}", country.capital);
        println!("");
        return country.is_correct;
    }
}
